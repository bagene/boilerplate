A simple crud operations using Laravel and Vue.js used as the boilerplate for a web app.

# Installation Dependencies
### Run the following command

composer install

php artisan passport:install

# Configuration
### run the following command

cp env.example .env

php artisan key:generate

### Database connection

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=boiler
DB_USERNAME=root
DB_PASSWORD=[your password]

### Run

php artisan serve

Go to http://localhost:8000

### Admin

email: admin@example.com
password: admin