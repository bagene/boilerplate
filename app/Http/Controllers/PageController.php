<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __constructor(){
      $this->middleware('auth');
    }

    public function categories(){
      return view('categories');
    }

    public function post(){
      return view('post');
    }
}
