<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['name','description','user_id','category_id'];

    public function images(){
      return $this->hasMany(PostImage::class);
    }
}
