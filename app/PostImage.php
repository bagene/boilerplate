<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    protected $fillable = ['filename','url','post_id'];
    public $timestamps = false;
}
